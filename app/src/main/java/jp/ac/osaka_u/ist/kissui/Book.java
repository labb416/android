package jp.ac.osaka_u.ist.kissui;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by k-yokoi on 2016/09/04.
 */
public class Book implements Parcelable {
    //constant
    public static final int book_segment = 4;
    public static final Creator<Book> CREATOR = new Creator<Book>() {
        @Override
        public Book createFromParcel(Parcel in) {
            return new Book(in);
        }

        @Override
        public Book[] newArray(int size) {
            return new Book[size];
        }
    };
    private static final int frameInterval = 0;
    //global variable
    private boolean isSet = true;
    private int step = 0;
    private int pageCount = 0;
    private int pageCountIncrease = 1;
    private int pageCountRate = 3;
    private int frameCount = 0;

    public Book() {}

    protected Book(Parcel in) {
        isSet = in.readByte() != 1;
        step = in.readInt();
        pageCount = in.readInt();
        pageCountRate = in.readInt();
        pageCountIncrease = in.readInt();
        frameCount = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (isSet ? 1 : 0));
        dest.writeInt(step);
        dest.writeInt(pageCount);
        dest.writeInt(pageCountRate);
        dest.writeInt(pageCountIncrease);
        dest.writeInt(frameCount);
    }

    //method
    public boolean update(){
        if(step > 0) {
            frameCount++;
            if(frameCount>frameInterval){
                step++;
                frameCount=0;
                if(step >book_segment) step = 0 ;
            }

        }
        return true;
    }

    public boolean read(int status, int preStatus, int mode) {
        mode=Math.max(mode,1);
        if (!isSet || step > 0) return false;
        step = 1;
        if (status == 4 && preStatus == 3) pageCount += pageCountRate * pageCountIncrease * mode;
        else pageCount += pageCountIncrease * mode;
        return  true;
    }

    public void hide(){
        isSet = false;
    }

    public void show(){
        isSet = true;
    }

    //getter
    public boolean isSet(){
        return isSet;
    }

    public boolean setIsSet(boolean value) {
        return isSet = value;
    }


    public int getStep(){
        return step;
    }

    public int getPageCount(){
        return pageCount;
    }

    public int getPageCountRate() {
        return pageCountRate;
    }

    public void setPageCountRate(int value) {
        pageCountRate = value;
    }

    public int getPageCountIncrease() {
        return pageCountIncrease;
    }

    public void setPageCountIncrease(int value) {
        pageCountIncrease = value;
    }
}
