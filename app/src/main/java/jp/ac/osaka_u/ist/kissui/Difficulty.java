package jp.ac.osaka_u.ist.kissui;


import android.os.Parcel;
import android.os.Parcelable;

public class Difficulty implements Parcelable {
    /** フレーム数 */
    private int now_frame = 0;
    /**
     * 現在の難度レベル
     */
    private int level = 1;

    private int fRate = 300;

    private Teacher teacher;
    private Tension tension;
    private Book book;

    public Difficulty(Book book,Teacher teacher, Tension tension){
        this.book=book;
        this.teacher=teacher;
        this.tension=tension;
    }

    protected Difficulty(Parcel in) {
        level = in.readInt();
        now_frame = in.readInt();
        fRate = in.readInt();
        teacher = in.readParcelable(Teacher.class.getClassLoader());
        tension = in.readParcelable(Tension.class.getClassLoader());
        book = in.readParcelable(Tension.class.getClassLoader());
    }

    public static final Creator<Difficulty> CREATOR = new Creator<Difficulty>() {
        @Override
        public Difficulty createFromParcel(Parcel in) {
            return new Difficulty(in);
        }

        @Override
        public Difficulty[] newArray(int size) {
            return new Difficulty[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(level);
        dest.writeInt(now_frame);
        dest.writeInt(fRate);
        dest.writeParcelable(teacher, flags);
        dest.writeParcelable(tension, flags);
        dest.writeParcelable(book, flags);
    }

    public void setValue(int level) {
        switch (level) {
            case 1:
                teacher.setPro2(2, 1);
                teacher.setPro3(4, 3);
                teacher.setPro4(300, 299);
                teacher.setWaitFrame(30);
                teacher.setDenom123(6);
                teacher.setDenom45(3);
                tension.setRecoveryRate(1);
                book.setPageCountRate(3);
                tension.setDamage(2);
                tension.setRecovery(20);
                book.setPageCountIncrease(1);
                break;
            case 2:
                teacher.setPro2(2, 1);
                teacher.setPro3(5, 3);
                teacher.setPro4(300, 299);
                teacher.setWaitFrame(30);
                teacher.setDenom123(6);
                teacher.setDenom45(3);
                tension.setRecoveryRate(1);
                book.setPageCountRate(3);
                tension.setDamage(2);
                tension.setRecovery(20);
                book.setPageCountIncrease(2);
                break;
            case 3:
                teacher.setPro2(2, 1);
                teacher.setPro3(2, 1);
                teacher.setPro4(300, 299);
                teacher.setWaitFrame(30);
                teacher.setDenom123(6);
                teacher.setDenom45(3);
                tension.setRecoveryRate(1);
                book.setPageCountRate(3);
                tension.setDamage(2);
                tension.setRecovery(20);
                book.setPageCountIncrease(3);
                break;
            case 4:
                teacher.setPro2(5, 2);
                teacher.setPro3(2, 1);
                teacher.setPro4(300, 299);
                teacher.setDenom123(5);
                teacher.setDenom45(4);
                teacher.setWaitFrame(25);
                tension.setRecoveryRate(2);
                book.setPageCountRate(4);
                tension.setDamage(3);
                tension.setRecovery(20);
                book.setPageCountIncrease(4);
                break;
            case 5:
                teacher.setPro2(5, 2);
                teacher.setPro3(2, 1);
                teacher.setPro4(300, 299);
                teacher.setDenom123(5);
                teacher.setDenom45(4);
                teacher.setWaitFrame(25);
                tension.setRecoveryRate(2);
                book.setPageCountRate(4);
                tension.setDamage(3);
                tension.setRecovery(20);
                book.setPageCountIncrease(5);
                break;
            case 6:
                teacher.setPro2(3, 1);
                teacher.setPro3(2, 1);
                teacher.setPro4(300, 299);
                teacher.setDenom123(5);
                teacher.setDenom45(4);
                teacher.setWaitFrame(25);
                tension.setRecoveryRate(2);
                book.setPageCountRate(4);
                tension.setDamage(3);
                tension.setRecovery(20);
                book.setPageCountIncrease(6);
                break;
            case 7:
                teacher.setPro2(3, 1);
                teacher.setPro3(5, 2);
                teacher.setPro4(100, 99);
                teacher.setDenom123(4);
                teacher.setDenom45(4);
                teacher.setWaitFrame(20);
                tension.setRecoveryRate(2);
                book.setPageCountRate(4);
                tension.setDamage(3);
                tension.setRecovery(20);
                book.setPageCountIncrease(7);
                break;
            case 8:
                teacher.setPro2(4, 1);
                teacher.setPro3(5, 2);
                teacher.setPro4(80, 79);
                teacher.setDenom123(4);
                teacher.setDenom45(3);
                teacher.setWaitFrame(20);
                tension.setRecoveryRate(3);
                book.setPageCountRate(5);
                tension.setDamage(4);
                tension.setRecovery(20);
                book.setPageCountIncrease(8);
                break;
            case 9:
                teacher.setPro2(4, 1);
                teacher.setPro3(3, 1);
                teacher.setPro4(80, 79);
                teacher.setDenom123(3);
                teacher.setDenom45(3);
                teacher.setWaitFrame(20);
                tension.setRecoveryRate(3);
                book.setPageCountRate(5);
                tension.setDamage(4);
                tension.setRecovery(20);
                book.setPageCountIncrease(9);
                break;
            case 10:
                teacher.setPro2(4, 1);
                teacher.setPro3(3, 1);
                teacher.setPro4(80, 79);
                teacher.setDenom123(2);
                teacher.setDenom45(2);
                teacher.setWaitFrame(20);
                tension.setRecoveryRate(3);
                book.setPageCountRate(6);
                tension.setDamage(5);
                tension.setRecovery(20);
                book.setPageCountIncrease(10);
                break;
            case 11:
                teacher.setPro4(50, 49);
                teacher.setWaitFrame(15);
                tension.setRecoveryRate(3);
                book.setPageCountRate(10);
                tension.setDamage(6);
                tension.setRecovery(25);
                book.setPageCountIncrease(15);
                break;
            case 12:
                teacher.setPro4(20, 19);
                tension.setDamage(7);
                book.setPageCountRate(13);
                book.setPageCountIncrease(19);
                break;
            case 13:
                teacher.setPro4(20, 19);
                tension.setDamage(8);
                book.setPageCountRate(15);
                book.setPageCountIncrease(23);
                break;
            case 14:
                teacher.setPro2(1, 0);
                teacher.setPro3(1, 0);
                teacher.setPro4(10, 9);
                tension.setDamage(9);
                book.setPageCountRate(17);
                book.setPageCountIncrease(27);
                break;
            case 15:
                teacher.setPro2(1, 0);
                teacher.setPro3(1, 0);
                teacher.setPro4(6, 5);
                teacher.setDenom123(2);
                teacher.setDenom45(2);
                teacher.setDenom123(3);
                teacher.setDenom45(3);
                teacher.setWaitFrame(13);
                tension.setRecoveryRate(3);
                tension.setDamage(10);
                tension.setRecovery(20);
                book.setPageCountRate(19);
                book.setPageCountIncrease(31);
                break;
            default:
                teacher.setPro4(3, 2);
                tension.setDamage(10 + (level - 15) / 3);
                book.setPageCountRate(book.getPageCountRate() + 3);
                book.setPageCountIncrease(book.getPageCountRate() + 5);
                break;
        }
    }

    void update() {
        if (now_frame % 30 == 0 && levelChecker()) {
            setLevel(level + 1);
        }
        now_frame++;
    }

    public boolean levelChecker() {
        if (now_frame == 0) return false;
        //TODO 超適当
        /*
        if (now_frame == 1 * fRate) {
            return true;
        } else if (now_frame == 2 * fRate) {
            return true;
        } else if (now_frame == 3 * fRate) {
            return true;
        } else if (now_frame == 4 * fRate) {
            return true;
        } else if (now_frame == 5 * fRate) {
            return true;
        } else if (now_frame == 6 * fRate) {
            return true;
        } else if (now_frame == 7 * fRate) {
            return true;
        } else if (now_frame == 8 * fRate) {
            return true;
        } else if (now_frame == 9 * fRate) {
            return true;
        } else if (now_frame == 10 * fRate) {
            return true;
        }
        */
        else if (now_frame % fRate == 0) {
            return true;
        }
        return false;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
        setValue(level);
    }

    public void setFRate(int fRate) {
        this.fRate = fRate;
    }
}
