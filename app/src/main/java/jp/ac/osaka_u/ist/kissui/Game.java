package jp.ac.osaka_u.ist.kissui;

import android.media.SoundPool;
import android.os.Parcel;
import android.os.Parcelable;

import static jp.ac.osaka_u.ist.kissui.Mode.*;

/**
 * Created by yuya on 2016/09/06.
 */
public class Game implements Parcelable {
    public static final Creator<Game> CREATOR = new Creator<Game>() {
        @Override
        public Game createFromParcel(Parcel in) {
            return new Game(in);
        }

        @Override
        public Game[] newArray(int size) {
            return new Game[size];
        }
    };
    public static SoundPool mSoundPool;
    private Tension tension = new Tension();
    private Teacher teacher = new Teacher();
    private Book book = new Book();
    private Difficulty difficulty = new Difficulty(book,teacher,tension);
    private boolean isFinished =false;
    private boolean isStarted = false;
    private int mode=EASY_MODE;
    private int pageSoundId, hideSoundId;
    private boolean isSound;
    private boolean isDebug;

    public Game() {}

    protected Game(Parcel in) {
        tension = in.readParcelable(Tension.class.getClassLoader());
        teacher = in.readParcelable(Teacher.class.getClassLoader());
        difficulty = in.readParcelable(Difficulty.class.getClassLoader());
        book = in.readParcelable(Book.class.getClassLoader());
        mode = in.readInt();
        isStarted = in.readByte() != 0;
        isFinished = in.readByte() != 0;
        pageSoundId = in.readInt();
        hideSoundId = in.readInt();
        isSound = in.readByte() != 0;
        isDebug = in.readByte() != 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(tension, flags);
        dest.writeParcelable(teacher, flags);
        dest.writeParcelable(difficulty, flags);
        dest.writeParcelable(book, flags);
        dest.writeInt(mode);
        dest.writeByte((byte) (isStarted ? 1 : 0));
        dest.writeByte((byte) (isFinished ? 1 : 0));
        dest.writeInt(pageSoundId);
        dest.writeInt(hideSoundId);
        dest.writeByte((byte) (isSound ? 1 : 0));
        dest.writeByte((byte) (isDebug ? 1 : 0));
    }

    public void updateFrame(Direction flickDirection){
        if(!isStarted) return;
        if ((teacher.isDangerous() && book.isSet() && !isDebug) || tension.isZero()) {
            isFinished =true;
            return;
        }
        switch (flickDirection){
            case UP:
                if (book.isSet()) playHide();
                book.hide();
                break;
            case DOWN:
                if (!book.isSet()) playHide();
                book.show();
                break;
            case RIGHT:
                if (book.read(teacher.getStatus(), teacher.getPreStatus(), mode)) {
                    playPage();
                    tension.heal(teacher.getStatus(), teacher.getPreStatus());
                }
                break;
            case LEFT:
                if (isDebug) {
                    if (book.read(teacher.getStatus(), teacher.getPreStatus(), mode)) {
                        tension.damage();
                    }
                }
                break;

        }
        //book.update();
        difficulty.update();
        teacher.update();
        if (!isDebug) tension.update(book.isSet(), mode);
    }

    public void finish() {
        mSoundPool.unload(pageSoundId);
        mSoundPool.unload(hideSoundId);
        mSoundPool.release();
        mSoundPool = null;
    }

    public boolean isFinished(){
        return isFinished;
    }

    public boolean isStarted() {
        return isStarted;
    }

    public void setStarted(boolean isStarted) {
        this.isStarted = isStarted;
    }

    public void setMode(int mode, boolean isSound, GameActivity gameActivity) {
        mSoundPool = Sound.createPool(5);
        pageSoundId = mSoundPool.load(gameActivity.getApplicationContext(), R.raw.page, 0);
        hideSoundId = mSoundPool.load(gameActivity.getApplicationContext(), R.raw.hide, 0);
        this.mode=mode;
        this.isSound = isSound;
        isDebug = Mode.isDebug(mode);
        if (isDebug) {
            difficulty.setLevel(15);
            difficulty.setFRate(450);
            teacher.setIsFeint(true);
            teacher.setNowDenom();
            book.setIsSet(false);//通常本はtrueスタート
        } else if (mode == SEVERE_MODE) {
            //初期レベル
            difficulty.setLevel(8);
            difficulty.setFRate(600);
            teacher.setIsFeint(true);
            teacher.setNowDenom();
        } else if (mode == EASY_MODE) {
            //初期レベル
            difficulty.setLevel(1);
            difficulty.setFRate(450);
            teacher.setIsFeint(false);
            teacher.setNowDenom();
        }
    }

    private void playPage() {
        if (!isSound) return;
        mSoundPool.play(pageSoundId, 0.8F, 0.8F, 0, 0, 1.0F);
    }

    private void playHide() {
        if (!isSound) return;
        mSoundPool.play(hideSoundId, 0.4F, 0.4F, 0, 0, 1.0F);
    }

    public Book getBook() {
        return book;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public Tension getTension() {
        return tension;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }
}
