package jp.ac.osaka_u.ist.kissui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class GameActivity extends AppCompatActivity {

    public static final int FPS = 30;
    static final long FRAME_TIME = 1000 / FPS;
    private static final int START = 60;
    private static final int END = 120;
    int frame;
    int count;
    float frameRate;
    private volatile Game game;
    private SharedPreferences sp;
    private MediaPlayer mp;
    private long baseTime;
    private int topRecord;
    private int currentRecord;
    private int mode;
    private int endFrame = -1;
    private boolean isSound;
    private volatile Timer timer;
    private volatile Direction flickDirection;

    private GameSurfaceView surfaceView;
    private SoundPool mSoundPool;
    private int gaanSoundId;
    private int chimeSoundId;

    private int firstFrame = 60;
    private int isSoundLoad = 0;
    private boolean isGameStarted = false;
    private boolean isGameOngoing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        surfaceView = new GameSurfaceView(this);
        setContentView(surfaceView);

        Intent intent = getIntent();
        mode = intent.getIntExtra("mode", 1);
        isSound = intent.getBooleanExtra("sound", true);
        String pref_name="test_pref";
        sp = getSharedPreferences(pref_name, MODE_PRIVATE);
        baseTime = System.currentTimeMillis();

        flickDirection = Direction.NONE;

        if (savedInstanceState != null) {
            game = savedInstanceState.getParcelable("game");
            frame = savedInstanceState.getInt("frame");
            endFrame = savedInstanceState.getInt("endFrame");
            isGameOngoing = savedInstanceState.getBoolean("isGameOngoing");
        } else {
            game = new Game();
        }
        game.setMode(mode, isSound, this);
        surfaceView.setModel(this, game, game.getBook(), game.getTeacher(), game.getTension(), game.getDifficulty());
        surfaceView.setMode(mode);
        surfaceView.setOnTouchListener(new OnFlickListener() {
            @Override
            public void onFlick(Direction direction) {
                flickDirection = direction;
            }
        });

        if(isSound){
            setMediaPlayer();
            setSoundPool();
        }else{
            mp = null;
            isGameStarted=true;
            game.setStarted(true);
        }

        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                updateFrame();
            }
        }, 0, FRAME_TIME);
    }

    private void setMediaPlayer(){
        AssetFileDescriptor afDescriptor;
        mp = new MediaPlayer();
        try {
            afDescriptor = getAssets().openFd("bgm.mp3");
            mp.setDataSource(afDescriptor.getFileDescriptor(), afDescriptor.getStartOffset(),
                    afDescriptor.getLength());
        } catch (IOException e) {
            e.printStackTrace();
        }
        mp.setVolume(0.7f, 0.7f);
        mp.setLooping(true);
        try {
            mp.prepare();
        } catch (IllegalStateException | IOException e) {
            e.printStackTrace();
        }
    }

    private void setSoundPool(){
        mSoundPool = Sound.createPool(5);
        chimeSoundId = mSoundPool.load(getApplicationContext(), R.raw.chime, 0);
        gaanSoundId = mSoundPool.load(getApplicationContext(), R.raw.gaan, 1);
        mSoundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                if (status != 0) return;
                if (isSound) playSoundPool(chimeSoundId);
                isSoundLoad=1;
            }
        });
    }

    private void playSoundPool(int Id) {
        if (!isSound) return;
        try {
            mSoundPool.play(Id, 0.7F, 0.7F, 0, 0, 1.0F);
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    private void updateFrame() {
        if(firstFrame>0){
            firstFrame--;
            return;
        }
        if(isSound){
            if(isSoundLoad == 1){
                isGameStarted=true;
                game.setStarted(true);
                isSoundLoad = 2;
            }
            if(isSoundLoad == 2 && frame >= START && mp!=null){
                mp.start();
            }
        }
        if (!isGameOngoing || !isGameStarted) return;
        if (game.isFinished()) {
            if (endFrame == -1) gameFinishNow();
            if (frame - endFrame >= END) {//ゲーム終了時120フレームだけ待つ

                finishGame();
                return;
            }
        }

        calFPS();
        if (game.isStarted()) {
            game.updateFrame(flickDirection);
        }
        flickDirection = Direction.NONE;
        frame++;
    }

    private void gameFinishNow(){
        try {
            if (isSound) {
                mp.stop();
                mp.release();
                mp = null;
            }
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        playSoundPool(gaanSoundId);
        endFrame = frame;
    }

    private void finishGame() {
        timer.cancel();

        //record
        currentRecord = game.getBook().getPageCount();
        int recordMode = sp.getInt("record" + String.valueOf(mode), -1);
        SharedPreferences.Editor e = sp.edit();
        topRecord = Math.max(recordMode, game.getBook().getPageCount());
        e.putInt("record" + String.valueOf(mode), topRecord);
        e.apply();
        int sum =sp.getInt("sum", -1);
        e.putInt("sum", sum+game.getBook().getPageCount());
        e.apply();

        //soundPool
        try {
            mSoundPool.unload(gaanSoundId);
            mSoundPool.unload(chimeSoundId);
            mSoundPool.release();
        }catch (NullPointerException ex){
            ex.printStackTrace();
        }
        mSoundPool = null;

        //gameActivityEnd
        isGameOngoing=false;

        //gameEnd
        game.finish();

        //surfaceEnd
        surfaceView.surfaceDestroyed(surfaceView.getHolder());

        //startGameOver
        startGameOverActivity();
    }

    private void startGameOverActivity() {
        Intent intent = new Intent(getApplication(), GameOverActivity.class);
        intent.putExtra("mode", mode);
        intent.putExtra("sound", isSound);
        intent.putExtra("topRecord", topRecord);
        intent.putExtra("currentRecord", currentRecord);
        startActivity(intent);
        finish();
    }

    private void calFPS() {
        count++;
        long now = System.currentTimeMillis();      //現在時刻を取得
        if (now - baseTime >= 1000) {       //１秒以上経過していれば
            frameRate = (float) (count * 1000) / (float) (now - baseTime);        //フレームレートを計算
            baseTime = now;     //現在時刻を基準時間に
            count = 0;          //フレーム数をリセット
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isGameOngoing = true;
        if (mp != null) {
            if (!mp.isPlaying() && isSound && frame > 60 && !game.isFinished()) {
                mp.start();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isGameOngoing = false;
        if (mp != null) {
            if (mp.isPlaying()) {
                mp.pause();
            }
        }
    }

    public boolean getGameOngoing() {
        return isGameOngoing;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("game", game);
        outState.putInt("frame", frame);
        outState.putInt("endFrame", endFrame);
        outState.putBoolean("isGameOngoing", isGameOngoing);
    }
}
