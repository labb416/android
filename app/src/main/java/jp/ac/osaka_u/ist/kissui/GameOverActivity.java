package jp.ac.osaka_u.ist.kissui;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class GameOverActivity extends AppCompatActivity {
    private int mode;
    private boolean isSound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gameover);

        Button restartButton = (Button) findViewById(R.id.restart_button);
        Button titleButton = (Button) findViewById(R.id.title_button);
        ImageButton shareButton = (ImageButton)findViewById(R.id.share_button);

        TextView record_text = (TextView) findViewById(R.id.record_text);

        Intent intent = getIntent();
        mode = intent.getIntExtra("mode", 1);
        isSound = intent.getBooleanExtra("sound", true);
        final int record = intent.getIntExtra("currentRecord", 0);
        int topRecord = intent.getIntExtra("topRecord", 0);

        if (topRecord != record) {
            record_text.setText(getString(R.string.current_record_text, record));
            record_text.setTextColor(Color.BLACK);
        } else {
            record_text.setText(getString(R.string.top_record_text, record));
            record_text.setTextColor(Color.RED);
        }

        restartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 二度押し防止
                if (v.isEnabled()) {
                    v.setEnabled(false);
                    startGameActivity();
                }
            }
        });

        titleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 二度押し防止
                if (v.isEnabled()) {
                    v.setEnabled(false);
                    startMainActivity();
                }
            }
        });
       shareButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // 二度押し防止
                if (v.isEnabled()) {
                    v.setEnabled(false);
                    Intent i = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://twitter.com/share/?text=マンガを" + record + "ページ読んだ。 先生にバレずにマンガを読みまくれ！" +
                                    "&url=https://play.google.com/store/apps/details?id=jp.ac.osaka_u.ist.kissui" +
                                    "&hashtags=きっすいくん"
                            ));
                    startActivity(i);
                    v.setEnabled(true);
                }

            }
        });
    }

    private void startGameActivity() {
        Intent intent = new Intent(getApplication(), GameActivity.class);
        intent.putExtra("mode", mode);
        intent.putExtra("sound", isSound);
        startActivity(intent);
        finish();
    }

    private void startMainActivity() {
        Intent intent = new Intent(getApplication(), MainActivity.class);
        startActivity(intent);
        finish();
    }
}
