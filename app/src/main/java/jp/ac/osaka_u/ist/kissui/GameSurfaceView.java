package jp.ac.osaka_u.ist.kissui;

/**
 * Created by k-yokoi on 2016/09/14.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.PorterDuff;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.SurfaceHolder.Callback;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.Random;

import static jp.ac.osaka_u.ist.kissui.Mode.*;

public class GameSurfaceView extends SurfaceView implements Callback, Runnable {
    static final long FPS = 30;
    static final long FRAME_TIME = 1000 / FPS;
    private final Bitmap[] bookImages = new Bitmap[Book.book_segment + 1];
    private final Bitmap[] teacherImages = new Bitmap[5 * 2];
    SurfaceHolder surfaceHolder;
    public Thread thread;
    Bitmap bookImage = null;
    Bitmap teacherImage = null;
    Canvas canvas = null;
    Paint tensionBar = new Paint();
    Paint tensionBarBack = new Paint();
    Paint bgPaint = new Paint();
    Paint underZone = new Paint();
    Paint aboveZone = new Paint();
    Paint bitmapPaint = new Paint();
    Rect src = new Rect();
    Rect dst = new Rect();
    private int para[] = {196, 144, 100, 64, 36, 16, 4, 0, 3, 12, 27, 48, 27, 12, 3, 0, 2, 8, 18, 8, 2, 0, 1, 2, 1, 0, 1, 0, 1, 0};
    private Bitmap blackboardImage;
    private Bitmap deskImage1, deskImage2;
    private Bitmap teacherDeskImage, startImage, endImage;
    private GameActivity gameActivity;
    private Game game;
    private Book book;
    private Teacher teacher;
    private Tension tension;
    private Difficulty difficulty;
    private Random rnd = new Random();
    private int previousStatus;
    private boolean isReversed;
    private int book_place;
    private boolean teacherTeleport;
    private int viewCount, count, startCount = 8, endCount = -1;
    private int h, w;
    private long baseTime;
    private float frameRate;
    private int screen_width, screen_height,width,height;
    private int book_height;
    private int screen_left, screen_right ,screen_top, screen_bottom;
    private int bookX, bookY, teacherX, teacherY, barTopY, barBottomY;
    private int mode;
    private boolean destroy=false;
    private DecimalFormat df = new DecimalFormat("0.0");


    public GameSurfaceView(Context context) {
        super(context);
        surfaceHolder = getHolder();
        //callbackメソッド（下の３つ）を登録
        surfaceHolder.addCallback(this);
    }

    public void setModel(GameActivity gameActivity, Game game, Book book, Teacher teacher, Tension tension, Difficulty difficulty) {
        this.gameActivity = gameActivity;
        this.game = game;
        this.book = book;
        this.teacher = teacher;
        this.tension = tension;
        this.difficulty = difficulty;
    }



    @Override
    public void run() {
        tensionBar.setStyle(Style.FILL);
        tensionBarBack.setColor(Color.parseColor("#33CCCC"));
        aboveZone.setColor(Color.parseColor("#FFFBED"));
        underZone.setColor(Color.parseColor("#FFFBED"));
        bgPaint.setColor(Color.parseColor("#FFFBED"));
        long loopCount = 0;
        long startTime = System.currentTimeMillis();
        bitmapPaint.setFilterBitmap(true);
        while(thread!=null){
            try{
                viewCount++;
                count++;
                long now = System.currentTimeMillis();      //現在時刻を取得
                if (now - baseTime >= 1000)
                {       //１秒以上経過していれば
                    frameRate = (float) (count * 1000) / (float) (now - baseTime);        //フレームレートを計算
                    baseTime = now;     //現在時刻を基準時間に
                    count = 0;          //フレーム数をリセット
                }
                loopCount++;
                if (gameActivity.getGameOngoing()) {//不要？
                    canvas = surfaceHolder.lockCanvas();
                    // ここから画面更新処理
                    if(canvas!=null) canvas.drawColor(0, PorterDuff.Mode.CLEAR);//リセット
                    if(canvas!=null) canvas.drawRect(0, 0, width, height, bgPaint);//背景
                    drawUnderBar();
                    drawAboveBar();
                    drawBar();
                    drawLogo();
                    // ここまで画面更新処理
                    if(canvas!=null) surfaceHolder.unlockCanvasAndPost(canvas);
                }
                //wait処理
                long waitTime = (loopCount * FRAME_TIME) - (System.currentTimeMillis() - startTime);
                if (waitTime > 0) {
                    Thread.sleep(waitTime);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void drawAboveBar() {
        if(canvas!=null) canvas.drawRect(screen_left, screen_top, screen_right, barTopY, aboveZone);//上部
        //黒板
        if (blackboardImage != null) {
            w = blackboardImage.getWidth();
            h = blackboardImage.getHeight();
            src.set(0, 0, w, h);
            dst.set(screen_left, screen_top
                    , screen_right, screen_top + screen_width * h / w);
            if(canvas!=null) canvas.drawBitmap(blackboardImage, src, dst, null);
        }

        if (mode == DEBUG_MODE) {
            Paint textPaint = new Paint();
            textPaint.setTextSize(height / 33);
            textPaint.setColor(Color.WHITE);
            if(canvas!=null) canvas.drawText("DebugMode", screen_left + screen_width / 20, screen_top + height / 33, textPaint);
            if(canvas!=null) canvas.drawText("GameFPS=" + df.format(gameActivity.frameRate) + " SurfaceFPS=" + df.format(frameRate)
                    , screen_left + screen_width / 20, screen_top + height / 33 * 2, textPaint);
        }

        //先生
        int status = teacher.getStatus();
        //リバース
        if (status == 4 && previousStatus == 5) {
            if (rnd.nextInt(3) != 0) isReversed = !isReversed;
        }
        // テレポート
        if (status == 4 && previousStatus == 5) {
            //ステータスが４の時、描画スレッドでの前ステータスが５の時
            // （これは後の画像選択の便宜上 Teacher.statusは3になっている、ここはpreviousStatusでなければならない）
            // フェイントが行われる時、２分の１の確率でテレポート、残りの確率で５の方向を向いた４
            // ややこしいので、拡張するならちゃんと整理しなければならないが、今のところ動く
            if (teacher.isDoFeint() && rnd.nextInt(2) == 0) teacherTeleport = true;
            else teacherTeleport = false;
        } else if (status == 4 && previousStatus == 3) {
            //ステータスが４の時、前ステータスが３、フェイントがONになっている時、レベルが１１以上のとき、20分の１の確率で
            if (teacher.isFeint() && difficulty.getLevel() > 10 && rnd.nextInt(20) == 0)
                teacherTeleport = true;
            else teacherTeleport = false;
        }
        //テレポートここまで
        if (status == 5) {
            teacherImage = teacherImages[4];
        } else if (!isReversed) {
            if (status == 4 && teacher.getPreStatus() == 5) {
                teacherImage = teacherImages[status - 1 + 5];
            } else {
                teacherImage = teacherImages[status - 1];
            }
        } else {//isReversed
            if (status == 4 && teacher.getPreStatus() == 5) {
                teacherImage = teacherImages[status - 1];
            } else {
                teacherImage = teacherImages[status - 1 + 5];
            }
        }

        if (mode == TEACHER_MODE) testTeacherDraw();

        else if (teacherImage != null) {
            h = teacherImage.getHeight();
            w = teacherImage.getWidth();
            Rect t_src = new Rect(0, 0, w, h);
            Rect t_dst = new Rect();
            int teacherImageHeight = (barTopY - screen_top);
            int teacherImageWidth = w * teacherImageHeight / h;


            if (status == 5) {
                t_dst.set(screen_left + screen_width / 2 - teacherImageWidth / 2, teacherY - teacherImageHeight / 7,
                        screen_left + screen_width / 2 + teacherImageWidth / 2 - 1, teacherY + teacherImageHeight - 1 - teacherImageHeight / 7);
            } else {
                if (!isReversed) {
                    t_dst.set(teacherX - teacherImageWidth / 2, teacherY - teacherImageHeight / 15,
                            teacherX + teacherImageWidth / 2 - 1, teacherY + teacherImageHeight - 1 - teacherImageHeight / 15);
                    switch (status) {
                        case 1:
                        case 2:
                            break;
                        case 3:
                            t_dst.left = screen_left + screen_width / 5 - teacherImageWidth / 2;
                            t_dst.right = screen_left + screen_width / 5 + teacherImageWidth / 2 - 1;
                            break;
                        case 4:
                            t_dst.left = screen_left + screen_width / 3 - teacherImageWidth / 2;
                            t_dst.right = screen_left + screen_width / 3 + teacherImageWidth / 2 - 1;
                            break;
                        default:
                            break;
                    }
                } else {
                    t_dst.set((width - teacherX) - teacherImageWidth / 2, teacherY - teacherImageHeight / 15,
                            (width - teacherX) + teacherImageWidth / 2 - 1, teacherY + teacherImageHeight - 1 - teacherImageHeight / 15);
                    switch (status) {
                        case 1:
                        case 2:
                            break;
                        case 3:
                            t_dst.left = screen_right - screen_width / 5 - teacherImageWidth / 2;
                            t_dst.right = screen_right - screen_width / 5 + teacherImageWidth / 2 - 1;
                            break;
                        case 4:
                            t_dst.left = screen_right - screen_width / 3 - teacherImageWidth / 2;
                            t_dst.right = screen_right - screen_width / 3 + teacherImageWidth / 2 - 1;
                            break;
                        default:
                            break;
                    }
                }
            }
            if (!(teacherTeleport && (status == 4 || status == 5)) && mode != TEACHER_MODE) {
                if(canvas!=null) canvas.drawBitmap(teacherImage, t_src, t_dst, bitmapPaint);
            }
        }
        previousStatus = status;

        //教卓
        if (teacherDeskImage != null) {
            w = teacherDeskImage.getWidth();
            h = teacherDeskImage.getHeight();
            src.set(0, 0, w, h / 2);
            dst.set(width / 2 - screen_width / 6, screen_top + screen_height * 2 / 9
                    , width / 2 + screen_width / 6, screen_top + screen_height * 2 / 9 + screen_width / 3 * h / w * 4 / 3);
            if(canvas!=null) canvas.drawBitmap(teacherDeskImage, src, dst, null);
        }
    }

    public void testTeacherDraw() {
        //test表示用 エミュnexus9用
        int teacherImageHeight;
        int teacherImageWidth;
        Rect t_src = new Rect(0, 0, w, h);
        Rect t_dst = new Rect();

        if (teacherImages[4] != null) {
            h = teacherImages[4].getHeight();
            w = teacherImages[4].getWidth();
            teacherImageHeight = (barTopY - screen_top);
            teacherImageWidth = w * teacherImageHeight / h;
            t_src = new Rect(0, 0, w, h);
            t_dst.set(screen_left + screen_width / 2 - teacherImageWidth / 2, teacherY - teacherImageHeight / 7,
                    screen_left + screen_width / 2 + teacherImageWidth / 2 - 1, teacherY + teacherImageHeight - 1 - teacherImageHeight / 7);
            if(canvas!=null) canvas.drawBitmap(teacherImages[4], t_src, t_dst, bitmapPaint);
        }

        if (teacherImages[2] != null) {
            h = teacherImages[2].getHeight();
            w = teacherImages[2].getWidth();
            teacherImageHeight = (barTopY - screen_top);
            teacherImageWidth = w * teacherImageHeight / h;
            t_src = new Rect(0, 0, w, h);
            t_dst.set((width - teacherX) - teacherImageWidth / 2, teacherY - teacherImageHeight / 15,
                    (width - teacherX) + teacherImageWidth / 2 - 1, teacherY + teacherImageHeight - 1 - teacherImageHeight / 15);
            t_dst.left = screen_left - teacherImageWidth / 2;
            t_dst.right = screen_left + teacherImageWidth / 2 - 1;
            if(canvas!=null) canvas.drawBitmap(teacherImages[2], t_src, t_dst, bitmapPaint);
        }
        if (teacherImages[3] != null) {
            h = teacherImages[3].getHeight();
            w = teacherImages[3].getWidth();
            teacherImageHeight = (barTopY - screen_top);
            teacherImageWidth = w * teacherImageHeight / h;
            t_src = new Rect(0, 0, w, h);
            t_dst.set((width - teacherX) - teacherImageWidth / 2, teacherY - teacherImageHeight / 15,
                    (width - teacherX) + teacherImageWidth / 2 - 1, teacherY + teacherImageHeight - 1 - teacherImageHeight / 15);
            t_dst.left = screen_left + screen_width / 4 - teacherImageWidth / 2;
            t_dst.right = screen_left + screen_width / 4 + teacherImageWidth / 2 - 1;
            if(canvas!=null) canvas.drawBitmap(teacherImages[3], t_src, t_dst, bitmapPaint);
        }
        if (teacherImages[7] != null) {
            h = teacherImages[7].getHeight();
            w = teacherImages[7].getWidth();
            teacherImageHeight = (barTopY - screen_top);
            teacherImageWidth = w * teacherImageHeight / h;
            t_src = new Rect(0, 0, w, h);
            t_dst.set((width - teacherX) - teacherImageWidth / 2, teacherY - teacherImageHeight / 15,
                    (width - teacherX) + teacherImageWidth / 2 - 1, teacherY + teacherImageHeight - 1 - teacherImageHeight / 15);
            t_dst.left = screen_right - teacherImageWidth / 2;
            t_dst.right = screen_right + teacherImageWidth / 2 - 1;
            if(canvas!=null) canvas.drawBitmap(teacherImages[7], t_src, t_dst, bitmapPaint);
        }

        if (teacherImages[8] != null) {
            h = teacherImages[8].getHeight();
            w = teacherImages[8].getWidth();
            teacherImageHeight = (barTopY - screen_top);
            teacherImageWidth = w * teacherImageHeight / h;
            t_src = new Rect(0, 0, w, h);
            t_dst.set((width - teacherX) - teacherImageWidth / 2, teacherY - teacherImageHeight / 15,
                    (width - teacherX) + teacherImageWidth / 2 - 1, teacherY + teacherImageHeight - 1 - teacherImageHeight / 15);
            t_dst.left = screen_right - screen_width / 4 - teacherImageWidth / 2;
            t_dst.right = screen_right - screen_width / 4 + teacherImageWidth / 2 - 1;
            if(canvas!=null) canvas.drawBitmap(teacherImages[8], t_src, t_dst, bitmapPaint);
        }
    }

    public void drawBar() {
        //レベル表示
        String str;
        if (difficulty.getLevel() >= 16) str = "Level:" + difficulty.getLevel() + "(超シビア)";
        else if (difficulty.getLevel() >= 12) str = "Level:" + difficulty.getLevel() + "(シビア)";
        else str = "Level:" + String.valueOf(difficulty.getLevel());
        Paint levelPaint = new Paint();
        levelPaint.setTextSize(height / 20);
        float testWidth = levelPaint.measureText(str);
        levelPaint.setColor(Color.WHITE);
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                if (i == 0 && j == 0) continue;
                if(canvas!=null) canvas.drawText(str,
                        width - testWidth + i * 2, barBottomY - height * 3 / 40 + j * 2, levelPaint);
            }
        }
        levelPaint.setColor(Color.RED);
        if(canvas!=null) canvas.drawText(str,
                width - testWidth, barBottomY - height * 3 / 40, levelPaint);

        //テレポート先生
        if (teacherImage != null && teacherTeleport && teacher.getStatus() == 5) {
            Rect t_src = new Rect(0, 0, w, h);
            Rect t_dst = new Rect();
            h = teacherImage.getHeight();
            w = teacherImage.getWidth();
            int teacherImageHeight = (barTopY - screen_top);
            int teacherImageWidth = w * teacherImageHeight / h;
            t_src.set(0, 0, w, h / 4);
            t_dst.set(width / 2 - teacherImageWidth * 5 / 4, teacherY + screen_height / 8,
                    width / 2 + teacherImageWidth * 5 / 4, teacherY + screen_height / 8 + teacherImageHeight * 5 / 8 - 1);
            if(canvas!=null) canvas.drawBitmap(teacherImage, t_src, t_dst, bitmapPaint);
        }

        //バー
        double tensionRate = (double) tension.getValue() / (double) tension.getMax();
        if (tensionRate > 0.6) {
            tensionBar.setColor(Color.BLUE);
        } else if (tensionRate > 0.3) {
            tensionBar.setColor(Color.YELLOW);
        } else {
            tensionBar.setColor(Color.RED);
        }
        int tension_width = width * tension.getValue() / tension.getMax();

        if(canvas!=null) canvas.drawRect(0, barTopY - 3, width, barBottomY + 4, tensionBarBack);
        if(canvas!=null) canvas.drawRect(0, barTopY, tension_width, barBottomY - 1, tensionBar);

        //ページ
        String page = String.valueOf(book.getPageCount()) + "ページ";
        Paint pagePaint = new Paint();
        pagePaint.setTextSize(height / 20);
        testWidth = pagePaint.measureText(page);
        pagePaint.setColor(Color.WHITE);
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                if (i == 0 && j == 0) continue;
                if(canvas!=null) canvas.drawText(page,
                        width - testWidth + i * 2, barBottomY - 4 + j * 2, pagePaint);
            }
        }
        pagePaint.setColor(Color.BLUE);
        if(canvas!=null) canvas.drawText(page,
                width - testWidth, barBottomY - 4, pagePaint);
    }

    public void drawUnderBar() {
        if(canvas!=null) canvas.drawRect(screen_left, barBottomY, screen_right, screen_bottom, underZone);//下部

        //机下
        if (deskImage2 != null) {
            w = deskImage2.getWidth();
            h = deskImage2.getHeight();
            src.set(0, 0, w, h);
            dst.set(width / 2 - screen_width * 3 / 5, screen_top + screen_height * 2 / 9
                    , width / 2 + screen_width * 3 / 5, screen_top + screen_height * 2 / 9 + screen_width * 6 / 5 * h / w);
            if(canvas!=null) canvas.drawBitmap(deskImage2, src, dst, null);
        }

        //本説
        bookImage = bookImages[book.getStep()];
        book.update();
        if (bookImage != null) {
            if (book.isSet()) book_place--;
            else book_place++;
            book_place = Math.max(book_place, 0);
            book_place = Math.min(book_place, 4);
            Rect b_src = new Rect(0, 0, 600, 600);
            Rect b_dst = new Rect(bookX, bookY - book_height * 5 / 6 * book_place / 4
                    , bookX + book_height, bookY + book_height - book_height * 5 / 6 * book_place / 4);
            if(canvas!=null) canvas.drawBitmap(bookImage, b_src, b_dst, null);
        }

        //机上
        if (deskImage1 != null) {
            dst.top = dst.top + 3;
            dst.bottom = dst.bottom + 3;//調整
            if(canvas!=null) canvas.drawBitmap(deskImage1, src, dst, null);
        }

    }

    public void drawLogo() {
        //開始ロゴ
        if (startImage != null) {
            if (!game.isStarted()) {
                w = startImage.getWidth();
                h = startImage.getHeight();
                src.set(0, 0, w, h);
                dst.left = screen_left;
                dst.top = height / 3 - screen_width * h / w / 2;
                dst.right = screen_right;
                dst.bottom = height / 3 + screen_width * h / w / 2;
                if(canvas!=null) canvas.drawBitmap(startImage, src, dst, null);
            }
            if (game.isStarted() && startCount > 0) {
                w = startImage.getWidth();
                h = startImage.getHeight();
                src.set(0, 0, w, h);
                dst.left = screen_left;
                dst.top = height / 3 * startCount / 8 - screen_width * h / w / 2;
                dst.right = screen_right;
                dst.bottom = height / 3 * startCount / 8 + screen_width * h / w / 2;
                if(canvas!=null) canvas.drawBitmap(startImage, src, dst, null);
                startCount--;
            }
        }
        //終了ロゴ
        if (game.isFinished() && endImage != null) {
            if (endCount == -1) endCount = viewCount;
            w = endImage.getWidth();
            h = endImage.getHeight();
            src.set(0, 0, w, h);
            int index = Math.min(viewCount - endCount, para.length - 1);
            dst.left = screen_left;
            dst.top = height / 3 - screen_width * h / w / 2 - para[index] * height / 2 / para[0];
            dst.right = screen_right;
            dst.bottom = height / 3 + screen_width * h / w / 2 - para[index] * height / 2 / para[0];
            if(canvas!=null) canvas.drawBitmap(endImage, src, dst, null);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height) {
        this.width=width;
        this.height=height;
        if (width < height * 5 / 8) {//縦長
            screen_width=width;
            screen_height = screen_width * 8 / 5;
            screen_top=height/2-screen_height/2;
            screen_bottom=height/2+screen_height/2;
            screen_left=0;
            screen_right=screen_width;
        }else{//横長
            screen_height = height;
            screen_width = height * 5 / 8;
            screen_left =width/2-screen_width/2;
            screen_right =width/2+screen_width/2;
            screen_top=0;
            screen_bottom=screen_height;
        }

        teacherX = screen_left + screen_width / 10;
        teacherY = screen_top + screen_height / 16;
        barTopY = screen_top + screen_height * 10 / 20;
        barBottomY = screen_top + screen_height * 11 / 20;
        bookY = (barBottomY * 5 + screen_bottom) / 6;
        book_height = (screen_bottom - bookY) * 6 / 5;
        bookX = screen_left+screen_width/2 - book_height/2;
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {


        for (int i = 0; i < bookImages.length; ++i) {
            bookImages[i] = getImage("book" + i + ".png");
        }
        for (int i = 0; i < teacherImages.length / 2; ++i) {
            teacherImages[i] = getImage("teacher" + i + ".png");
            if (teacherImages[i] != null) {
                /*
                Matrix matrix = new Matrix();
                matrix.preScale(0.5f, 0.5f);
                teacherImages[i] = Bitmap.createBitmap(teacherImages[i], 0, 0,
                        teacherImages[i].getWidth(), teacherImages[i].getHeight(), matrix, false);
                */
                Matrix matrix2 = new Matrix();
                matrix2.preScale(-1, 1);
                teacherImages[i + teacherImages.length / 2] = Bitmap.createBitmap(teacherImages[i], 0, 0,
                        teacherImages[i].getWidth(), teacherImages[i].getHeight(), matrix2, false);
            } else {
                teacherImages[i + teacherImages.length / 2] = null;
            }
        }
        deskImage1 = getImage("Desk1.png");
        deskImage2 = getImage("Desk2.png");
        teacherDeskImage = getImage("TeachersDesk.png");
        blackboardImage = getImage("BlackBoard.png");
        startImage = getImage("start.png");
        endImage = getImage("end.png");

        thread = new Thread(this);
        thread.start();

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        for (int i = 0; i < bookImages.length; ++i) {
            if (bookImages[i] != null) bookImages[i].recycle();
            bookImages[i] = null;
        }

        for (int i = 0; i < teacherImages.length; ++i) {
            if (teacherImages[i] != null) teacherImages[i].recycle();
            teacherImages[i] = null;
        }
        if (deskImage1 != null) deskImage1.recycle();
        deskImage1 = null;
        if (deskImage2 != null) deskImage2.recycle();
        deskImage1 = null;
        if (teacherDeskImage != null) teacherDeskImage.recycle();
        teacherDeskImage = null;
        if (blackboardImage != null) blackboardImage.recycle();
        blackboardImage = null;
        if (startImage != null) startImage.recycle();
        startImage = null;
        if (endImage != null) endImage.recycle();
        endImage = null;
        thread = null;
    }

    public Bitmap getImage(String str) {
        InputStream stream = null;
        Bitmap image;
        try {
            stream = getResources().getAssets().open(str);
            image = BitmapFactory.decodeStream(stream);
        } catch (IOException ex) {
            image = null;
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return image;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

}
