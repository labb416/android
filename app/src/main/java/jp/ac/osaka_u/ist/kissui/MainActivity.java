package jp.ac.osaka_u.ist.kissui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.ImageView;

import static jp.ac.osaka_u.ist.kissui.Mode.*;
import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity {

    private SharedPreferences sp;
    private TextView recordText, versionText;
    private boolean isSound = true;
    private MediaPlayer mp;
    private ImageView titleImage;
    private Bitmap title_bitmap;
    private PopupWindow helpWindow;
    private MyDialogFragment dialogFragment;
    private boolean bornFlg = false;
    private int debugCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        titleImage = (ImageView) findViewById(R.id.title);

        final Button startButtonE = (Button) findViewById(R.id.start_button_easy);
        final Button startButtonS = (Button) findViewById(R.id.start_button_severe);
        Button resetButton = (Button) findViewById(R.id.reset_button);
        final Button helpButton = (Button) findViewById(R.id.help_button);
        final Button soundButton = (Button) findViewById(R.id.sound_button);
        recordText = (TextView) findViewById(R.id.record_text);
        versionText = (TextView) findViewById(R.id.version_text);

        String pref_name = "test_pref";
        sp = getSharedPreferences(pref_name, MODE_PRIVATE);

        isSound = sp.getBoolean("sound", true);
        if (isSound) {
            soundButton.setText(R.string.sound_button_off);
            mpSet();
        } else {
            soundButton.setText(R.string.sound_button_on);
            mp = null;
        }

        if (sp.getInt("sum", 0) != 0) {
            recordText.setText(getString(R.string.record_text,
                    sp.getInt("record1", 0), sp.getInt("record2", 0), sp.getInt("sum", 0)));
        } else {
            SharedPreferences.Editor e = sp.edit();
            recordText.setText(R.string.initial_record_text);
            e.putInt("record1", 0);
            e.apply();
            e.putInt("record2", 0);
            e.apply();
            e.putInt("sum", 0);
            e.apply();
        }
        versionText.setText(getString(R.string.version_text, getVersionName(this)));

        startButtonS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 二度押し防止
                if (v.isEnabled()) {
                    v.setEnabled(false);
                    startGameActivity(SEVERE_MODE);
                }
            }
        });
        startButtonE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 二度押し防止
                if (v.isEnabled()) {
                    v.setEnabled(false);
                    startGameActivity(EASY_MODE);
                }
            }
        });

        helpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 二度押し防止
                if (v.isEnabled()) {
                    v.setEnabled(false);

                    helpWindow = new PopupWindow(MainActivity.this);

                    // レイアウト設定
                    View popupView = getLayoutInflater().inflate(R.layout.help_window, null);
                    popupView.findViewById(R.id.close_button).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (v.isEnabled() && helpWindow.isShowing()) {
                                v.setEnabled(false);
                                helpButton.setEnabled(true);
                                helpWindow.dismiss();
                            }
                        }
                    });
                    helpWindow.setContentView(popupView);

                    // 背景設定
                    helpWindow.setBackgroundDrawable(new ColorDrawable(0xEEFFFFFF));

                    // タップ時に他のViewでキャッチされないための設定
                    helpWindow.setOutsideTouchable(false);
                    helpWindow.setFocusable(false);

                    // 表示サイズの設定 今回は幅300dp
                    //float width = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 320, getResources().getDisplayMetrics());
                    helpWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
                    helpWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

                    // 画面中央に表示
                    helpWindow.showAtLocation(findViewById(R.id.help_button), Gravity.CENTER, 0, 0);
                }
            }

        });


         soundButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 二度押し防止
                if (v.isEnabled()) {
                    v.setEnabled(false);
                    if(isSound) {
                        isSound=false;
                        soundButton.setText(R.string.sound_button_on);
                        mp.stop();
                        mp.release();
                        mp = null;
                    }else{
                        isSound=true;
                        soundButton.setText(R.string.sound_button_off);
                        mpSet();
                        if (mp != null) {
                            mp.start();
                        }
                    }
                    SharedPreferences.Editor e = sp.edit();
                    e.putBoolean("sound",isSound);
                    e.apply();
                    if (debugCount >= 2) {
                        debugCount += 1000;
                        startButtonE.setText(R.string.debug_button);
                        startButtonS.setText(R.string.teacher_button);
                        recordText.setText(getString(R.string.initial_record_text2, debugCount));
                    }
                }
                v.setEnabled(true);
            }
        });

        dialogFragment = MyDialogFragment
                .newInstance(R.string.dialog_title, R.string.dialog_message);
        dialogFragment.setOnOkClickListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!isSound && bornFlg) {
                    debugCount++;
                } else {
                    debugCount = 0;
                }
                bornFlg = true;
                if (debugCount < 3) recordText.setText(R.string.initial_record_text);
                else recordText.setText(getString(R.string.initial_record_text2, debugCount));
            }
        });
        dialogFragment.setOnCancelClickListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                debugCount = 0;
            }
        });

        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.isEnabled()) {
                    v.setEnabled(false);
                    dialogFragment.show(getFragmentManager(), "dialog_fragment");
                }
                v.setEnabled(true);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mp != null) mp.start();

        title_bitmap = getImage("kissui_title.png");
        if (title_bitmap != null) titleImage.setImageBitmap(title_bitmap);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mp != null) {
            if (mp.isPlaying()) {
                mp.pause();
            }
        }

        title_bitmap.recycle();
        title_bitmap = null;
    }

    @Override
    protected void onDestroy() {
        if (helpWindow != null && helpWindow.isShowing()) {
            helpWindow.dismiss();
        }
        super.onDestroy();
    }

    private void mpSet() {
        mp = new MediaPlayer();
        try {
            AssetFileDescriptor afDescriptor = getAssets().openFd("op.mp3");
            mp.setDataSource(afDescriptor.getFileDescriptor(), afDescriptor.getStartOffset(),
                    afDescriptor.getLength());
        } catch (IOException e) {
            e.printStackTrace();
        }
        mp.setLooping(true);
        mp.setVolume(0.7f, 0.7f);
        try {
            mp.prepare();
        } catch (IllegalStateException | IOException e) {
            e.printStackTrace();
        }
    }

    private void startGameActivity(int mode) {
        if (mode == EASY_MODE && debugCount > 1000) {
            mode = DEBUG_MODE;
        } else if (mode == SEVERE_MODE && debugCount > 1000) {
            mode = TEACHER_MODE;
        } else if (bornFlg) {
            SharedPreferences.Editor e = sp.edit();
            e.putInt("record1", 0);
            e.apply();
            e.putInt("record2", 0);
            e.apply();
            e.putInt("sum", 0);
            e.apply();
        }
        Intent intent=new Intent(getApplication(), GameActivity.class);
        intent.putExtra("mode", mode);
        intent.putExtra("sound", isSound);
        if (isSound && mp != null) {
            mp.stop();
            mp.release();
            mp = null;
        }
        startActivity(intent);
        finish();
    }

    private Bitmap getImage(String str) {
        InputStream stream = null;
        Bitmap image;
        try {
            stream = getResources().getAssets().open(str);
            image = BitmapFactory.decodeStream(stream);
        } catch (IOException ex) {
            image = null;
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return image;
    }

    private String getVersionName(Context context) {
        PackageManager pm = context.getPackageManager();
        String versionName = "";
        try {
            PackageInfo packageInfo = pm.getPackageInfo(context.getPackageName(), 0);
            versionName = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }
}
