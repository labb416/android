package jp.ac.osaka_u.ist.kissui;

public class Mode {
    public static final int TEACHER_MODE = -1;
    public static final int DEBUG_MODE = 0;
    public static final int EASY_MODE = 1;
    public static final int SEVERE_MODE = 2;

    private Mode() {
    }

    public static boolean isDebug(int mode) {
        return (mode == TEACHER_MODE || mode == DEBUG_MODE);
    }
}
