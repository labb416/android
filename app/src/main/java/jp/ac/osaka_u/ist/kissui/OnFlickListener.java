package jp.ac.osaka_u.ist.kissui;

import android.view.MotionEvent;
import android.view.View;

public abstract class OnFlickListener implements View.OnTouchListener {

    private float startX;
    private float startY;
    private float endX;
    private float endY;

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startX = event.getX();
                startY = event.getY();
                break;
            case MotionEvent.ACTION_UP:
                endX = event.getX();
                endY = event.getY();
                checkFlick(v);
                v.performClick();
                break;
            default:
                break;
        }
        return true;
    }

    private void checkFlick(View v) {
        float diffX = endX - startX;
        float diffY = endY - startY;
        int halfHeight = v.getHeight() / 2;
        if (startY < halfHeight && endY < halfHeight) {
            return;
        }
        if (Math.abs(diffX) >= Math.abs(diffY) * 2) {
            if (Math.abs(diffX) >= v.getWidth() / 30) {
                if (diffX > 0) onFlick(Direction.RIGHT);
                if (diffX < 0) onFlick(Direction.LEFT);
            }
        } else {
            if (Math.abs(diffY) > v.getWidth() / 15) {
                if (diffY > 0) onFlick(Direction.DOWN);
                if (diffY < 0) onFlick(Direction.UP);
            }
        }
    }

    public abstract void onFlick(Direction direction);
}
