package jp.ac.osaka_u.ist.kissui;

/**
 * Created by 雄一 on 2016/09/29.
 */

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.widget.TextView;

/**
 * フォントサイズ自動調整TextView
 */
public class ResizeTextView extends TextView {
    /**
     * コンストラクタ
     *
     * @param context
     */
    public ResizeTextView(Context context) {
        super(context);
    }

    /**
     * コンストラクタ
     *
     * @param context
     * @param attrs
     */
    public ResizeTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * 子Viewの位置を決める
     */
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        resize();
    }

    /**
     * テキストサイズ調整
     */
    private void resize() {
        /** 最小のテキストサイズ */
        final float MIN_TEXT_SIZE = 10f;

        int viewWidth = this.getWidth();    // Viewの横幅


        float textSize = getTextSize();

        // Paintにテキストサイズ設定
        Paint paint = new Paint();
        paint.setTextSize(textSize);
        FontMetrics fm = paint.getFontMetrics();

        // 最大テキスト抽出
        String str = getText().toString();
        String maxString = str;//最大文字列
        String[] strings = str.split("\n", 0);//一行に分割
        int l = 0;
        float textWidth = 0;
        while (l < strings.length) {
            if (textWidth < paint.measureText(strings[l])) {
                textWidth = paint.measureText(strings[l]);
                maxString = strings[l];
            }
            l++;
        }
        // テキストの横幅取得

        // 縦幅と、横幅が収まるまでループ
        while (viewWidth < textWidth) {
            // 調整しているテキストサイズが、定義している最小サイズ以下か。
            if (MIN_TEXT_SIZE >= textSize) {
                // 最小サイズ以下になる場合は最小サイズ
                textSize = MIN_TEXT_SIZE;
                break;
            }

            // テキストサイズをデクリメント
            textSize--;

            // Paintにテキストサイズ設定
            paint.setTextSize(textSize);

            // テキストの縦幅を再取得
            fm = paint.getFontMetrics();

            // テキストの横幅を再取得
            textWidth = paint.measureText(maxString);
        }

        // テキストサイズ設定
        setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
    }
}