package jp.ac.osaka_u.ist.kissui;

import android.annotation.TargetApi;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;

public class Sound {

    public static SoundPool createPool(int maxStreams) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return createNewPool(maxStreams);
        } else {
            return createOldPool(maxStreams);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static SoundPool createNewPool(int maxStreams) {
        AudioAttributes attr = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();
        return new SoundPool.Builder()
                .setAudioAttributes(attr)
                .setMaxStreams(maxStreams)
                .build();
    }

    @SuppressWarnings("deprecation")
    private static SoundPool createOldPool(int maxStreams) {
        return new SoundPool(maxStreams, AudioManager.STREAM_MUSIC, 0);
    }
}
