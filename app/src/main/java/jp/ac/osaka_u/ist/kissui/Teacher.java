package jp.ac.osaka_u.ist.kissui;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.Random;

public class Teacher implements Parcelable {
    /**
     * 1つ前の先生の状態
     */
    private int preStatus = 1;
    /** 現在の先生の状態 */
    private volatile int status = 3;
    /**
     * Status変更確率の分母　Status1-3
     */
    private int probDenom123 = 10;
    /**
     * Status変更確率の分母　Status45
     */
    private int probDenom45 = 3;
    /**
     * Status変更確率現在の分母
     */
    private int nowDenom = probDenom123;
    /** フレーム数 */
    private int nowFrame = 0;
    /** 前回行動時のフレーム数 */
    private int preFrame = 0;
    /** 1ターンのフレーム*/
    private int waitFrame = 30;
    /** ステータス変更可能になるまでの残りターン*/
    private int waitTurn = 0;
    /**
     * 待ちターン ステータス4
     */
    private int waitFourTurn = 2;
    /**
     * 待ちターン ステータス5
     */
    private int waitFiveTurn = 2;
    /**
     * フェイント有りかどうか
     */
    private boolean isFeint = false;
    /**
     * フェイントするかどうか
     */
    private boolean doFeint = false;
    /**
     * Status2 ステータス減少確率分母
     */
    private int pro2Denom = 3;
    /** Status2 ステータス減少確率分子*/
    private int pro2Numer = 1;
    /** Status2 ステータス減少確率分母*/
    private int pro3Denom = 5;
    /** Status2 ステータス減少確率分子*/
    private int pro3Numer = 2;
    /** Status2 ステータス減少確率分母*/
    private int pro4Denom = 2;
    /** Status2 ステータス減少確率分子*/
    private int pro4Numer = 1;

    /** ランダム関数のインスタンス */
    private Random rnd = new Random();

    public Teacher() {}

    protected Teacher(Parcel in) {
        preStatus = in.readInt();
        status = in.readInt();
        probDenom123 = in.readInt();
        nowDenom = in.readInt();
        nowFrame = in.readInt();
        preFrame = in.readInt();
        waitFrame = in.readInt();
        waitTurn = in.readInt();
        waitFourTurn = in.readInt();
        waitFiveTurn = in.readInt();
        isFeint = in.readByte() != 1;
        doFeint = in.readByte() != 1;
        pro2Denom = in.readInt();
        pro2Numer = in.readInt();
        pro3Denom = in.readInt();
        pro3Numer = in.readInt();
        pro4Denom = in.readInt();
        pro4Numer = in.readInt();
    }

    public static final Creator<Teacher> CREATOR = new Creator<Teacher>() {
        @Override
        public Teacher createFromParcel(Parcel in) {
            return new Teacher(in);
        }

        @Override
        public Teacher[] newArray(int size) {
            return new Teacher[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(preStatus);
        dest.writeInt(status);
        dest.writeInt(probDenom123);
        dest.writeInt(nowDenom);
        dest.writeInt(nowFrame);
        dest.writeInt(preFrame);
        dest.writeInt(waitFrame);
        dest.writeInt(waitTurn);
        dest.writeInt(waitFourTurn);
        dest.writeInt(waitFiveTurn);
        dest.writeByte((byte) (isFeint ? 1 : 0));
        dest.writeByte((byte) (doFeint ? 1 : 0));
        dest.writeInt(pro2Denom);
        dest.writeInt(pro2Numer);
        dest.writeInt(pro3Denom);
        dest.writeInt(pro3Numer);
        dest.writeInt(pro4Denom);
        dest.writeInt(pro4Numer);
    }

    /**
     * 一定時間ごとの処理を行う
     */
    public void update() {
        nowFrame++;
        if(nowFrame - preFrame >= waitFrame){
             calStatus();
             preFrame = nowFrame;
        }
    }

    /**
     * 現在のステータスの値を計算する
     * （現在適当だが『AI』なので要変更）
     */
    public void calStatus() {
        int ran;
        if (waitTurn > 0) {
            waitTurn--;
            return;
        }

        ran = rnd.nextInt(nowDenom);
        if (ran == 0) {
            if (status == 1) {
                status++;
                preStatus = 1;

            } else if (status == 2) {
                int ran2 = rnd.nextInt(pro2Denom);
                if (ran2 < pro2Numer) status--;
                else status++;
                preStatus = 2;

            } else if (status == 3) {
                int ran2 = rnd.nextInt(pro3Denom);
                if (ran2 < pro3Numer) status--;
                else {
                    status = 4;
                }
                preStatus = 3;

            } else if (status == 4) {
                if (!isFeint) {
                    if (preStatus == 3) status = 5;
                    else status = 3;
                } else {
                    if (preStatus == 3) status = 5;
                    else {
                        status = 3;
                    }
                }
                preStatus = 4;

            } else if (status == 5) {
                status = 4;
                if (isFeint) {
                    int ran2 = rnd.nextInt(pro4Denom);
                    if (ran2 < pro4Numer) {
                        status = 4;
                        doFeint = false;
                        waitTurn -= waitFourTurn;
                        preStatus = 5;
                    } else {
                        doFeint = true;
                        waitTurn++;
                        status = 4;
                        preStatus = 3;
                    }
                } else {
                    doFeint = false;
                    waitTurn -= waitFourTurn;
                    preStatus = 5;
                }
            }
            if (status > 0 && status < 4) {
                nowDenom = probDenom123;
            } else {
                nowDenom = probDenom45;
            }
            if (status == 4) {
                waitTurn += waitFourTurn;
            } else if (status == 5) {
                waitTurn += waitFiveTurn;
            }
        } else {
            nowDenom--;
        }


    }

    /**
     * 現在のステータスの値を取得する
     *
     * @return 現在のステータスの値
     */
    public int getStatus() {
        return status;
    }

    public int getPreStatus() {
        return preStatus;
    }
    /**
     * 現在のステータスの値を変更する
     *
     * @param value ステータスの値
     */
    public void setStatus(int value) {
        status = value;
    }

    public void setNowDenom() {
        this.nowDenom = probDenom123;
    }

    public void setWaitFrame(int value) {
        waitFrame = value;
    }

    public void setWaitFourTurn(int value) {
        waitFourTurn = value;
    }

    public void setWaitFiveTurn(int waitFiveTurn) {
        this.waitFiveTurn = waitFiveTurn;
    }

    public void setIsFeint(boolean value) {
        isFeint = value;
    }

    public boolean isFeint() {
        return isFeint;
    }

    /**分母先　分子後*/

    public void setPro2(int value,int value2) {
        pro2Denom = value;
        pro2Numer = value2;
    }

    public void setPro3(int value,int value2) {
        pro3Denom = value;
        pro3Numer = value2;
    }

    public void setPro4(int value,int value2) {
        pro4Denom = value;
        pro4Numer = value2;
    }

    /**
     * ステータスがデンジャラスか
     *
     * @return ステータス
     */
    public boolean isDangerous() {
        return status == 5;
    }

    public boolean isDoFeint() {
        return doFeint;
    }

    /**
     * ステータス変更の確率を変更する
     */
    public void setDenom123(int denom) {
        this.probDenom123 = denom;
    }

    public void setDenom45(int denom) {
        this.probDenom45 = denom;
    }
}