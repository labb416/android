package jp.ac.osaka_u.ist.kissui;

import android.os.Parcel;
import android.os.Parcelable;

public class Tension implements Parcelable {
    /** 現在のテンションの値 */
    private volatile int value = 100;
    /** テンションの最大値 */
    private volatile int max = 200;
    /** テンションの最小値 */
    private volatile int min = 0;
    /** 1回あたりのダメージ量 */
    private volatile int damage = 2;
    /** 1回あたりのダメージ倍率 */
    private volatile double damageRate = 2;
    /** 1ページあたりの回復量 */
    private volatile int recovery = 20;
    /** 1ページあたりの回復倍率 */
    private volatile double recoveryRate = 2;
    /** フレーム数 */
    private int nowFrame = 0;
    /** 前回行動時のフレーム数 */
    private int preFrame = 0;
    /** 1回減少時のフレーム数 */
    private int waitFrame = 10;

    public Tension() {}

    protected Tension(Parcel in) {
        value = in.readInt();
        max = in.readInt();
        damage = in.readInt();
        recovery = in.readInt();
        damageRate = in.readDouble();
        recoveryRate = in.readDouble();
        nowFrame = in.readInt();
        preFrame = in.readInt();
        waitFrame = in.readInt();
    }

    public static final Creator<Tension> CREATOR = new Creator<Tension>() {
        @Override
        public Tension createFromParcel(Parcel in) {
            return new Tension(in);
        }

        @Override
        public Tension[] newArray(int size) {
            return new Tension[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(value);
        dest.writeInt(max);
        dest.writeInt(damage);
        dest.writeInt(recovery);
        dest.writeDouble(damageRate);
        dest.writeDouble(recoveryRate);
        dest.writeInt(nowFrame);
        dest.writeInt(preFrame);
        dest.writeInt(waitFrame);
    }

    public void update(boolean isSetBook, int mode) {
        nowFrame++;
        if (mode != 0) mode = 1;
        if(nowFrame - preFrame >= waitFrame){
            if (!isSetBook) addValue(-(int) (damage * damageRate * mode));
            else addValue(-damage * mode);
            preFrame = nowFrame;
        }
    }
    /**
     * 回復する
     */
    public void heal(int status,int preStatus) {
        if(status == 4 && preStatus == 3)  addValue((int)(recovery*recoveryRate));
        else addValue(recovery);
    }

    public void damage() {
        addValue(-20);
    }

    /**
     * 現在のテンションの値を取得する
     * @return 現在のテンションの値
     */
    public int getValue() {
        return value;
    }

    /**
     * 現在のテンションの値を変更する
     * @param value テンションの値
     */
    public void setValue(int value) {
        value = Math.max(value, min);//0以下は0?
        value = Math.min(value, max);//100以上は100?
        this.value=value;
    }

    /**
     * 現在のテンションの値を増減する
     * @param value テンションの値
     */
    public void addValue(int value) {
        setValue(this.value + value);
    }

    /**
     * テンションの最大値を取得する
     * @return テンションの最大値
     */
    public int getMax() {
        return max;
    }

    /**
     * テンションの最大値を変更する
     * @param max テンションの最大値
     */
    public void setMax(int max) {
        this.max = max;
    }

    /**
     * 1回あたりのダメージ量を取得する
     * @return 1回あたりのダメージ量
     */
    public int getDamage() {
        return damage;
    }

    /**
     * 1回あたりのダメージ量を変更する
     * @param damageRate 1回あたりのダメージ量
     */

    public void setDamageRate(int damageRate) {
        this.damageRate = damageRate;
    }
    /**
     * 1回あたりのダメージ量を変更する
     * @param damage 1回あたりのダメージ量
     */
    public void setDamage(int damage) {
        this.damage = damage;
    }

    /**
     * 1ページあたりの回復量を取得する
     * @return 1ページあたりの回復量
     */
    public int getRecovery() {
        return recovery;
    }
    /**
     * 1ページあたりの回復量を取得する
     * @return 1ページあたりの回復量
     */
    public boolean isZero() {
        return value <= 0;
    }

    /**
     * 1ページあたりの回復量を変更する
     * @param recovery 1ページあたりの回復量
     */
    public void setRecovery(int recovery) {
        this.recovery = recovery;
    }

    public void setRecoveryRate(int recoveryRate) {
        this.recoveryRate = recoveryRate;
    }
}
